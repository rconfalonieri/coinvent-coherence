# Conceptual Coherence Implementation v0.1 #

This repository contains a first implementation for conceptual coherence using the OWL API and an Answer Set Programming solver. It implements conceptual coherence by Thagard, by means of the description logic **AL**.

### What is this repository for? ###

* An Eclipse project for running conceptual coherence
* Version: v0.1

### How do I get set up? ###

* You need to checkout the repository as a Java Maven project in Eclipse
* Dependencies: it depends on OWL API but the project uses Maven so that dependency should be automatically downloaded
* In the 'asp' folder you will get the result of coherence as a list of maximal partitions with their corresponding coherence value


### Who do I talk to? ###

* Roberto Confalonieri <**confalonieri** **AT** **iiia** **DOT** **csic** **DOT** **es**>

### References ###

* M. Schorlemmer, R. Confalonieri, E. Plaza. Coherent Conceptual Blending. Submitted to the C3GI
Computational Creativity, Concept Invention, and General Intelligence Workshop, 2016.
package eu.project.coinvent.coherence;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class ASPWrapper {


	@SuppressWarnings("finally")
	public static ArrayList<String> callASP(String aspLocation, String aspExe, String aspProblemEncoding, String aspEncoding ) {

		ArrayList<String> aspModelsToParse = null;
		String tmpModelString = null;
		String[] command = {aspLocation+aspExe, "--number=0","--configuration=auto","--opt-sat","--opt-mode=optN",aspLocation + aspProblemEncoding,aspLocation+"examples/"+aspEncoding};
		ProcessBuilder processBuilder = new ProcessBuilder(command);
		processBuilder.redirectErrorStream(true);
		//processBuilder.redirectOutput(new File("/tmp/aso.log"));
		try {
			Process p = processBuilder.start();

			//Read out dir output
			InputStream is = p.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;

			//System.out.printf("Output of running %s is:\n",Arrays.toString(command));
			boolean optimals = false;
			boolean toParse = false;

			while ((line = br.readLine()) != null) {
				//System.out.println(line);
				if (line.contentEquals("Answer: 1") && !optimals) 
					optimals = true;

				else if (line.contentEquals("Answer: 1") && optimals)  {
					toParse = true;
				}
				//parse optimal answer sets
				if (toParse) {
					tmpModelString = tmpModelString +"\n"+line;
				}


			}
			int exitValue = p.waitFor();
			System.out.println("\n\nExit Value is " + exitValue);

			//filter the solutions
			aspModelsToParse = new ArrayList<String>();
			String[] aspModels = tmpModelString.split("\n");
			for (int i=0; i<aspModels.length; i++) {
				if (aspModels[i].contains("in(") || aspModels[i].contains("out(")) {
					System.out.println("Saving asp model: "+aspModels[i]);
					aspModelsToParse.add(aspModels[i]); 
				}
			}


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			return aspModelsToParse;
		}

	}
}




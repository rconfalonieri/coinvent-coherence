package eu.project.coinvent.coherence;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.semanticweb.owlapi.model.OWLClassExpression;


public class CoherenceGraph {
	
	//private String coherenceGraphName;
	//private ArrayList<OWLClassExpression> nodes;
	private HashMap<OWLClassExpression, Integer> owlClassExToNodes;
	private HashMap<Integer, OWLClassExpression> nodesToOWLClassEx;
	private ArrayList<CoherenceRelation> coherenceRelations;
	private ArrayList<CoherenceRelation> inCoherenceRelations;
	private ArrayList<CoherenceSolution> coherenceSolutions;


	private String ontologyPrefix;

	public CoherenceGraph(ArrayList<OWLClassExpression> nodeList, String ontologyPrefix) {
		super();
		this.owlClassExToNodes = new HashMap<OWLClassExpression, Integer>();
		this.nodesToOWLClassEx = new HashMap<Integer, OWLClassExpression>();
		for (int i=0; i < nodeList.size(); i++) {
			owlClassExToNodes.put(nodeList.get(i), i+1);
			nodesToOWLClassEx.put(i+1, nodeList.get(i));
		}
		this.coherenceRelations = new ArrayList<CoherenceRelation>();
		this.inCoherenceRelations = new ArrayList<CoherenceRelation>();
		this.ontologyPrefix = ontologyPrefix;
	}



	public HashMap<OWLClassExpression, Integer> getNodes() {
		return owlClassExToNodes;
	}



	public void setNodes(HashMap<OWLClassExpression, Integer> nodes) {
		this.owlClassExToNodes = nodes;
	}



	public ArrayList<CoherenceRelation> getCoherenceRelations() {
		return coherenceRelations;
	}



	public void setCoherenceRelations(
			ArrayList<CoherenceRelation> coherenceRelations) {
		this.coherenceRelations = coherenceRelations;
	}



	public ArrayList<CoherenceRelation> getInCoherenceRelations() {
		return inCoherenceRelations;
	}



	public void setInCoherenceRelations(
			ArrayList<CoherenceRelation> inCoherenceRelations) {
		this.inCoherenceRelations = inCoherenceRelations;
	}



	@SuppressWarnings("rawtypes")
	public void toASPEncoding(String aspFile) throws FileNotFoundException, UnsupportedEncodingException {

		PrintWriter writer = new PrintWriter(aspFile, "UTF-8");
		Iterator iterator = owlClassExToNodes.entrySet().iterator();

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		writer.println("%%% AUTOMATICALLY GENERATED : "+dateFormat.format(date)+" do not change");
		writer.println();
		writer.println("% Nodes");
		while (iterator.hasNext()) {

			Map.Entry pair = (Map.Entry)iterator.next();
			//System.out.println(pair.getKey() + " = " + pair.getValue());
			String nodePrettryString = pair.getKey().toString().replace(ontologyPrefix, "").replace(">", "");
			writer.println("node("+pair.getValue()+").\t\t%%"+nodePrettryString);

		}
		writer.println();
		writer.println("% Coherence Relations");

		for (CoherenceRelation relation : coherenceRelations) {

			OWLClassExpression node1 = relation.getNode1();
			String node1PrettyString = node1.toString().replace(ontologyPrefix, "").replace(">", "");
			OWLClassExpression node2 = relation.getNode2();
			String node2PrettyString = node2.toString().replace(ontologyPrefix, "").replace(">", "");

			int degree = relation.getDegree();
			int nodeID1 = owlClassExToNodes.get(node1);
			int nodeID2 = owlClassExToNodes.get(node2);

			writer.println("coherence("+nodeID1+","+nodeID2+","+degree+").\t\t%% "+node1PrettyString+" coheres with "+node2PrettyString+ " with degree "+degree);

		}

		writer.println();
		writer.println("% Incoherence Relations");

		for (CoherenceRelation relation : inCoherenceRelations) {

			OWLClassExpression node1 = relation.getNode1();
			String node1PrettyString = node1.toString().replace(ontologyPrefix, "").replace(">", "");
			OWLClassExpression node2 = relation.getNode2();
			String node2PrettyString = node2.toString().replace(ontologyPrefix, "").replace(">", "");

			int degree = relation.getDegree();
			int nodeID1 = owlClassExToNodes.get(node1);
			int nodeID2 = owlClassExToNodes.get(node2);

			writer.println("coherence("+nodeID1+","+nodeID2+","+degree+").\t\t%% "+node1PrettyString+" incoheres with "+node2PrettyString+ " with degree "+degree);

		}


		writer.close();

	}



	public void fromASPEncoding(ArrayList<String> aspModels) {

		this.coherenceSolutions = new ArrayList<CoherenceSolution>();
		CoherenceSolution coherenceSolution;

		ArrayList<String> inNodes;
		ArrayList<String> outNodes;
		ArrayList<String> acceptedNodes;
		ArrayList<String> rejectedNodes;
		double coherenceDegree;

		String coherenceDegreeString = null;

		for (String aspModel : aspModels) {

			//System.out.println(aspModel);
			inNodes = new ArrayList<String>();
			outNodes = new ArrayList<String>();
			acceptedNodes = new ArrayList<String>();
			rejectedNodes = new ArrayList<String>();

			String[] facts = aspModel.split(" ");
			for (String fact : facts) {

				if (fact.startsWith("i")) {
					inNodes.add(fact);
				}
				else if (fact.startsWith("o")) {
					outNodes.add(fact);
				}
				else {
					coherenceDegreeString = fact;
				}

			}

			int nodeId;
			OWLClassExpression nodeIn;
			String nodeInPretty;
			String nodeOutPretty;
			OWLClassExpression nodeOut;


			for (String inNode : inNodes) {
				nodeId = Integer.parseInt(inNode.replace("in(", "").replace(")",""));
				nodeIn = this.nodesToOWLClassEx.get(nodeId);
				nodeInPretty = nodeIn.toString().replace(ontologyPrefix, "").replace(">", "");
				acceptedNodes.add(nodeInPretty);

			}
			for (String outNode : outNodes) {
				nodeId = Integer.parseInt(outNode.replace("out(", "").replace(")",""));
				nodeOut = this.nodesToOWLClassEx.get(nodeId);
				nodeOutPretty = nodeOut.toString().replace(ontologyPrefix, "").replace(">", "");
				rejectedNodes.add(nodeOutPretty);

			}
			double coherence = Double.parseDouble(coherenceDegreeString.replace("coherenceDegree(", "").replace(")", ""));
			coherenceDegree = coherence/100;

			coherenceSolution = new CoherenceSolution(acceptedNodes, rejectedNodes, coherenceDegree);
			coherenceSolutions.add(coherenceSolution);

			
		}//end for (String aspModel : aspModels)

	}



	public String showCoherencePartitions() {

		String finalResults = "NUMBER OF SOLUTIONS: " + coherenceSolutions.size()+"\n"; 
		int solNr = 1;
		
		for (CoherenceSolution coherenceSol : coherenceSolutions) {
			String result = "\t Solution "+solNr+":";
			String acceptedSet = "\t\tACCEPTED_SET = {";
			for (String acceptedNode: coherenceSol.getAcceptedNodes()) {
				acceptedSet = acceptedSet + acceptedNode+",";
			}
			if (coherenceSol.getAcceptedNodes().size()>0)
				acceptedSet = acceptedSet.substring(0, acceptedSet.length()-1)+"}";
			else 
				acceptedSet = acceptedSet +"}";
			
			String rejectedSet = "\t\tREJECTED_SET = {";
			for (String rejectedNode: coherenceSol.getRejectedNodes()) {
				rejectedSet = rejectedSet + rejectedNode+",";
			}
			if (coherenceSol.getRejectedNodes().size()>0)
				rejectedSet = rejectedSet.substring(0, rejectedSet.length()-1)+"}";
			else 
				rejectedSet = rejectedSet + "}";

			String coherenceDegree = "\t\tCOHERENCE_DEGREE = {"+String.valueOf(coherenceSol.getCoherenceDegree())+"}";

			//System.out.println("Result:");
			result = result +"\n"+ acceptedSet +"\n"+ rejectedSet +"\n"+ coherenceDegree +"\n";
			finalResults = finalResults + result;
			solNr++;
			//System.out.println(result);

		}
		return finalResults;
	}


}

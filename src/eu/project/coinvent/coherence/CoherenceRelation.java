package eu.project.coinvent.coherence;
import org.semanticweb.owlapi.model.OWLClassExpression;


public class CoherenceRelation {

	private OWLClassExpression node1;
	private OWLClassExpression node2;
	private int degree;
	
	
	
	
	public CoherenceRelation(OWLClassExpression node1,
			OWLClassExpression node2, int degree) {
		super();
		this.node1 = node1;
		this.node2 = node2;
		this.degree = degree;
	}
	
	
	public OWLClassExpression getNode1() {
		return node1;
	}
	public void setNode1(OWLClassExpression node1) {
		this.node1 = node1;
	}
	public OWLClassExpression getNode2() {
		return node2;
	}
	public void setNode2(OWLClassExpression node2) {
		this.node2 = node2;
	}
	public int getDegree() {
		return degree;
	}
	public void setDegree(int degree) {
		this.degree = degree;
	}
	
	
}

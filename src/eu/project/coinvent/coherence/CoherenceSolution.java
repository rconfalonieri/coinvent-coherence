package eu.project.coinvent.coherence;

import java.util.ArrayList;

public class CoherenceSolution {
	
	private ArrayList<String> acceptedNodes;
	private ArrayList<String> rejectedNodes;
	private double coherenceDegree;
	
	public CoherenceSolution() {
		
	}
	
	public CoherenceSolution(ArrayList<String> acceptedNodes,
			ArrayList<String> rejectedNodes, double coherenceDegree) {
		super();
		this.acceptedNodes = acceptedNodes;
		this.rejectedNodes = rejectedNodes;
		this.coherenceDegree = coherenceDegree;
	}

	public ArrayList<String> getAcceptedNodes() {
		return acceptedNodes;
	}

	public void setAcceptedNodes(ArrayList<String> acceptedNodes) {
		this.acceptedNodes = acceptedNodes;
	}

	public ArrayList<String> getRejectedNodes() {
		return rejectedNodes;
	}

	public void setRejectedNodes(ArrayList<String> rejectedNodes) {
		this.rejectedNodes = rejectedNodes;
	}

	public double getCoherenceDegree() {
		return coherenceDegree;
	}

	public void setCoherenceDegree(double coherenceDegree) {
		this.coherenceDegree = coherenceDegree;
	}
	
	
	

}

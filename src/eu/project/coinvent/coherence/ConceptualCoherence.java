package eu.project.coinvent.coherence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.StringDocumentSource;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.EntityType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.Searcher;
import org.semanticweb.owlapi.util.OWLOntologyWalker;
import org.semanticweb.owlapi.util.OWLOntologyWalkerVisitor;

import uk.ac.manchester.owl.owlapi.tutorial.ExistentialCollector;
import uk.ac.manchester.owl.owlapi.tutorial.SubClassCollector;

/**
 * 
 * Testing the OWL APIs
 * 
 * @author rconfalonieri
 *
 */
@SuppressWarnings("deprecation")
public class ConceptualCoherence {


	//private final String ontologyFile = "file:///Users/rconfalonieri/Documents/PostDoc/COINVENT/java/Coherence/examples/koala-ml.owl";
	private OWLDataFactory dataFactory;
	private OWLOntology ontology;
	private OWLReasoner reasoner;


	private ArrayList<OWLClassExpression> subConcepts;
	//private static CoherenceGraph coherenceGraph;
	private ArrayList<OWLClassExpressionPair> visitedPairs;
	private String ontologyName;
	private OWLClassExpression TOP;
	private OWLClassExpression BOTTOM;
	


	private OWLClassExpression input1Ex;
	private OWLClassExpression input2Ex;
	private String input1;
	private String input2;

	private String ontologyPrefix;
	private final int WEAK_COHERENCE = 70;
	private final int STRONG_COHERENCE = 100;
	private final int INCOHERENCE = -100;
	private final String coherenceObject = "Object";


	//private final static String ASPEncoding = "/Users/rconfalonieri/Documents/PostDoc/COINVENT/COINVENT-papers/coinvent-caos2016/implementation/examples";


	public ConceptualCoherence(String ontologyFileString, String input1, String input2, String ontologyprefix2) throws OWLOntologyCreationException {

		subConcepts = new ArrayList<OWLClassExpression>();
		dataFactory = OWLManager.getOWLDataFactory();
		this.ontologyPrefix = ontologyPrefix;
		//load an ontology
		File ontologyFile = new File(ontologyFileString);
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		IRI ontologyIRI = IRI.create(ontologyFile);
		ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
		System.out.println("Ontology Loaded...");
		//System.out.println("Ontology : " + ontology.getOntologyID());	
		ontologyName = ontology.getOntologyID().getOntologyIRI().get().toString();
		ontologyName = ontologyName.replace("http://www.semanticweb.org/ontologies/2016/3/", "").replace(".owl","");
		System.out.println("OntologyName: "+ontologyName);

		reasoner = new Reasoner.ReasonerFactory().createReasoner(ontology);
		TOP = dataFactory.getOWLThing();
		BOTTOM = dataFactory.getOWLNothing();

		this.input1 = input1;
		this.input2 = input2;
	}

	public void computeSubConceptOfBox() {
		OWLEntity entInput1 = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#"+input1));
		OWLEntity entInput2 = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#"+input2));

		for (OWLAxiom axiom: ontology.getTBoxAxioms(null)) {
			if (axiom.containsEntityInSignature(entInput1) || axiom.containsEntityInSignature(entInput2) )
				parse(axiom);
		}

		//We add the blended concept explicitly

		OWLClassExpression blend = dataFactory.getOWLObjectIntersectionOf(input1Ex,input2Ex);
		System.out.println("Blend :"+blend);
		subConcepts.add(blend);

		// We add the Bottom explicitly
		subConcepts.add(BOTTOM);

		//		OWLObjectIntersectionOf intersectionInput1 = null;
		//		OWLObjectIntersectionOf intersectionInput2 = null;
		//		
		//		for (OWLClass owlClass : ontology.getClassesInSignature()) {
		//			//System.out.println(owlClass);
		//			OWLEntity entInput1 = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#"+input1));
		//			
		//			//System.out.println(ent);
		//			Set<OWLClassExpression> superClassExpressions = new TreeSet<OWLClassExpression>();
		//			if (owlClass.containsEntityInSignature(entInput1)) {
		//				//System.out.println("yaaa1");
		//				//System.out.println("superclasses");
		//				for (OWLSubClassOfAxiom ax : ontology.getSubClassAxiomsForSubClass(owlClass)) {
		//					
		//					System.out.println(ax.getSuperClass());
		//					if (!ax.getSuperClass().toString().contains("#Object")) {
		//						superClassExpressions.add(ax.getSuperClass());
		//					}
		//				}
		//				
		//				intersectionInput1 = dataFactory.getOWLObjectIntersectionOf(superClassExpressions);
		//
		//			}
		//			superClassExpressions.clear();
		//			
		//			OWLEntity entInput2 = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#"+input2));
		//			
		//			if (owlClass.containsEntityInSignature(entInput2)) {
		//				//System.out.println("yaaa1");
		//				//System.out.println("superclasses");
		//				for (OWLSubClassOfAxiom ax : ontology.getSubClassAxiomsForSubClass(owlClass)) {
		//					
		//					System.out.println(ax.getSuperClass());
		//					if (!ax.getSuperClass().toString().contains("#Object")) {
		//						superClassExpressions.add(ax.getSuperClass());
		//					}
		//					
		//					
		//				}
		//				
		//				intersectionInput2 = dataFactory.getOWLObjectIntersectionOf(superClassExpressions);
		//
		//			}
		//			superClassExpressions.clear();
		//			
		//		}
		//		OWLObjectIntersectionOf blend = dataFactory.getOWLObjectIntersectionOf(intersectionInput1,intersectionInput2);
		//		subConcepts.add(blend);
		System.out.println("Number of nodes is: "+subConcepts.size());
		for (int i = 0; i < subConcepts.size(); i++) {
			System.out.println("Node: "+subConcepts.get(i));
		}

	}


	private void parse(OWLAxiom axiom) {

		//AxiomType<?> axiomType = axiom.getAxiomType();
		System.out.println("Axiom to parse :"+axiom.toString());
		
		
		Set<OWLClassExpression> expressions = axiom.getNestedClassExpressions();
		Iterator<OWLClassExpression> it = expressions.iterator();
		//OWLEntity objectEntity = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#Object"));
		OWLClassExpression object = dataFactory.getOWLClass(IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#"+coherenceObject));
		
		while (it.hasNext() ) {

			OWLClassExpression cex = it.next();
			System.out.println("Nested Expression: "+cex.toString());
			if (!containsPrimitiveConcepts(cex)) {
				if (!subConcepts.contains(cex) && !cex.equals(TOP)) {
					OWLObjectIntersectionOf intersection = dataFactory.getOWLObjectIntersectionOf(cex,object);
					subConcepts.add(intersection);
					if (cex.toString().contains("#"+input1))
						input1Ex = cex;
					if (cex.toString().contains("#"+input2))
						input2Ex = cex;

					
					//System.out.println("Expression: "+cex.toString()+ " added...");

				}
			}
			
		}	
	}
	
	private boolean containsPrimitiveConcepts(OWLClassExpression cex) {
			
		OWLEntity objectEntity = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#Object"));
		OWLEntity water = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#Water"));
		OWLEntity land = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#Land"));
		OWLEntity resident = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#Resident"));
		OWLEntity passenger = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#Passenger"));
		OWLEntity person = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#Person"));
		OWLEntity medium = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#Medium"));
		
		//return (cex.equals(objectEntity) );
		
		return (cex.equals(objectEntity) || cex.equals(water) || cex.equals(land) || cex.equals(resident) || cex.equals(passenger) || cex.equals(person) || cex.equals(medium));
		
	}

	
	public CoherenceGraph createCoherenceGraph(String ontologyprefix) {

		CoherenceGraph coherenceGraph = new CoherenceGraph(subConcepts, ontologyprefix);
		ArrayList<CoherenceRelation> coherenceRelations = new ArrayList<CoherenceRelation>();
		ArrayList<CoherenceRelation> inCoherenceRelations = new ArrayList<CoherenceRelation>();
		visitedPairs =  new ArrayList<OWLClassExpressionPair>();

		//for each pair of subconcepts, try whether they incohere or cohere
		for (int i=0; i<subConcepts.size();i++) {
			for (int j=0; j < subConcepts.size();j++) {

				if (!subConcepts.get(i).equals(subConcepts.get(j))) {

					//OWLEntity objectEntity = dataFactory.getOWLEntity(EntityType.CLASS, IRI.create("http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#Object"));
					//if (!subConcepts.get(i).equals(objectEntity) && !subConcepts.get(j).equals(objectEntity)) {
					//checking coherence (C and D) is satisfiable
					OWLClassExpression cls1 = subConcepts.get(i);
					//System.out.println("Node 1: "+cls1);
					OWLClassExpression cls2 = subConcepts.get(j);
					//System.out.println("Node 2: "+cls2);

					if  ( !visitedPair(cls1,cls2)) {

						try {
							CoherenceRelation incoherenceRelation = null;
							CoherenceRelation coherenceRelation = null;
							OWLSubClassOfAxiom subClassAxiom1;
							OWLSubClassOfAxiom subClassAxiom2;
							OWLEquivalentClassesAxiom equivalenceAx;
							OWLEquivalentClassesAxiom equivalenceAx1, equivalenceAx2;
							boolean weak_coherent1 = false;
							boolean weak_coherent2 = false;
							boolean equivalent = false;
							boolean equivalent2 = false;
							boolean incoherent = false;
							boolean satisfiable = false;
							boolean subsumed1 = false;
							boolean subsumed2 = false;

		
							OWLObjectIntersectionOf intersection = dataFactory.getOWLObjectIntersectionOf(cls1,cls2);								
							//System.out.println("*****INCOHERENCE CONDITION CHECK*********");
							//System.out.println("Axiom: "+intersection.toString()+" checking with reasoner....");
							satisfiable = reasoner.isSatisfiable(intersection);
							//System.out.println("Axiom is "+satisfiable);
							//System.out.println("*****INCOHERENCE CONDITION CHECK END*********");

							subClassAxiom1 = dataFactory.getOWLSubClassOfAxiom(cls1, cls2);
							//System.out.println("*****SUBSUME1 CHECK*********");
							//System.out.println("Axiom: "+subClassAxiom1.toString()+" checking with reasoner....");
							subsumed1 = reasoner.isEntailed(subClassAxiom1);
							//System.out.println("Axiom is "+subsumed1);
							//System.out.println("*****SUBSUME1 CHECK END*********");

							subClassAxiom2 = dataFactory.getOWLSubClassOfAxiom(cls2, cls1);
							//System.out.println("*****SUBSUME2 CHECK*********");
							//System.out.println("Axiom: "+subClassAxiom2.toString()+" checking with reasoner....");
							subsumed2 = reasoner.isEntailed(subClassAxiom2);
							//System.out.println("Axiom is "+subsumed2);
							//System.out.println("*****SUBSUME2 CHECK END*********");

							if (!satisfiable && !subsumed1 && !subsumed2) {
								incoherenceRelation = new CoherenceRelation(cls1, cls2, INCOHERENCE);
								inCoherenceRelations.add(incoherenceRelation);
								incoherent = true;
							}
							//}
							if (!incoherent) {

								equivalenceAx = dataFactory.getOWLEquivalentClassesAxiom(cls1,cls2);
								//System.out.println("*****EQUIVALENCE CHECK*********");
								//System.out.println("Axiom: "+equivalenceAx.toString()+" checking with reasoner....");
								equivalent = reasoner.isEntailed(equivalenceAx);
								//System.out.println("Equivalent: "+equivalent);
								//System.out.println("*****EQUIVALENCE CHECK END*********");

								if (equivalent) {
									coherenceRelation = new CoherenceRelation(cls1, cls2, STRONG_COHERENCE );
									coherenceRelations.add(coherenceRelation);
								}
								
								equivalenceAx1 = dataFactory.getOWLEquivalentClassesAxiom(cls1,BOTTOM);
								if (!equivalent && !cls1.isBottomEntity()) {
								//if (!equivalent && !reasoner.isEntailed(equivalenceAx1)) {

									//OWLObjectIntersectionOf intersection = dataFactory.getOWLObjectIntersectionOf(cls1,cls2);
									subClassAxiom1 = dataFactory.getOWLSubClassOfAxiom(cls1, cls2);
									weak_coherent1 = reasoner.isEntailed(subClassAxiom1);
									if (weak_coherent1) {
										coherenceRelation = new CoherenceRelation(cls1, cls2,WEAK_COHERENCE);
										coherenceRelations.add(coherenceRelation);

									}

								}
								equivalenceAx2 = dataFactory.getOWLEquivalentClassesAxiom(cls2,BOTTOM);
								if (!equivalent && !weak_coherent1 && !cls2.isBottomEntity()) {
								//if (!equivalent && !weak_coherent1 && !reasoner.isEntailed(equivalenceAx2)) {

									subClassAxiom2 = dataFactory.getOWLSubClassOfAxiom(cls2, cls1);
									weak_coherent2 = reasoner.isEntailed(subClassAxiom2);
									if (weak_coherent2) {
										coherenceRelation = new CoherenceRelation(cls2, cls1,WEAK_COHERENCE );
										coherenceRelations.add(coherenceRelation);

									}

								}

							}


						}
						catch(InconsistentOntologyException inconEx) {
							System.out.println(inconEx.toString());
							System.out.println("Ontology is inconsistent!");
						}
						catch(Exception ex) {
							System.out.println("Other exception");
							System.out.println(ex.toString());
						}

						//adding nodes to the visited ones
						OWLClassExpressionPair pair = new OWLClassExpressionPair(cls1, cls2);
						visitedPairs.add(pair);
					}
					else {
						//System.out.println("Nodes already visited...");
					}//end if  ( !visitedPair(cls1,cls2)) 

					//}//
				}//if (!subConcepts.get(i).equals(subConcepts.get(j))) {
			}// for (int j=0; j < subConcepts.size();j++) {
		}// for (int i=0; i < subConcepts.size();i++) {
		coherenceGraph.setCoherenceRelations(coherenceRelations);
		coherenceGraph.setInCoherenceRelations(inCoherenceRelations);
		//reasoner.dispose();
		return coherenceGraph;
	}



	private boolean visitedPair(OWLClassExpression cls1,
			OWLClassExpression cls2) {

		for (int i=0; i < visitedPairs.size();i++) {
			OWLClassExpressionPair pair = visitedPairs.get(i);

			if  ( (pair.getNode1().equals(cls1) && pair.getNode2().equals(cls2)) ||
					(pair.getNode1().equals(cls2) && pair.getNode2().equals(cls1)))
				return true;
		}
		return false;
	}





	public void checkConsistency() {
		//reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);
		boolean consistency = reasoner.isConsistent();

		System.out.println("The ontology is consistent :"+consistency);

	}

	public void disposeReasoner() {
		reasoner.dispose();	
	}

	public String getOntologyName() {
		// TODO Auto-generated method stub
		return this.ontologyName;
	}








}

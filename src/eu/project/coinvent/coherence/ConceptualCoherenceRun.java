package eu.project.coinvent.coherence;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public class ConceptualCoherenceRun {

	private final static String ontologyPath = "/Users/rconfalonieri/Documents/PostDoc/COINVENT/java/Coherence/examples/";
	private final static String ontologyPrefix = "<http://www.semanticweb.org/ontologies/2016/3/house-boat.owl#";

	private final static String[] inputOntologies1 = {"house","houseGEN1","houseGEN2","houseGEN3"};
	private final static String[] inputOntologies2 = {"boat","boatGEN1","boatGEN2","boatGEN3"};
	//private final static String[] inputOntologies1 = {"house"};
	//private final static String[] inputOntologies2 = {"boat"};

	//private final static String[] inputConcepts1 = {"House","HouseGEN3"};
	//private final static String[] inputConcepts2 = {"Boat","BoatGEN3"};
	private final static String[] inputConcepts1 = {"House","HouseGEN1","HouseGEN2","HouseGEN3"};
	private final static String[] inputConcepts2 = {"Boat","BoatGEN1","BoatGEN2","BoatGEN3"};


	private static CoherenceGraph coherenceGraph;
	private static HashMap<String,CoherenceGraph> coherenceGraphs;

	//private final static String resultsLocation = "/Users/rconfalonieri/Documents/PostDoc/COINVENT/COINVENT-papers/coinvent-caos2016/implementation/";
	private final static String aspLocation = "/Users/rconfalonieri/Documents/PostDoc/COINVENT/COINVENT-papers/coinvent-caos2016/implementation/";
	private final static String aspExe = "./clingo";
	private final static String aspProblemEncoding = "coherence.lp";

	@SuppressWarnings({"rawtypes" })
	public static void main(String[] args)  {

		try {


			String ontologyFile;

			String inputConcept1, inputConcept2;
			ConceptualCoherence coherence;
			coherenceGraphs = new HashMap<String, CoherenceGraph>();

			for (int i = 0; i < inputOntologies1.length; i++) {

				inputConcept1 = inputConcepts1[i];

				for (int j = 0; j < inputOntologies2.length; j++) {

					inputConcept2 = inputConcepts2[j];
					/*- The inputs  House and Boat (House-Boat)
					- The Generic Space (HouseGEN3-BoatGEN3)
					- The Houseboat blend (HouseGEN1-BoatGEN2)
					 */
					ontologyFile = ontologyPath + inputOntologies1[i]+"-"+inputOntologies2[j]+".owl";

					if (ontologyFile.equals(ontologyPath+"house-boat.owl") || ontologyFile.equals(ontologyPath+"houseGEN3-boatGEN3.owl")  || ontologyFile.equals(ontologyPath+"houseGEN1-boatGEN2.owl") ) {

						coherence = new ConceptualCoherence(ontologyFile,inputConcept1,inputConcept2,ontologyPrefix);
						coherence.computeSubConceptOfBox();

						coherence.checkConsistency();
						System.out.println("Creating coherence graph... :");
						coherenceGraph = coherence.createCoherenceGraph(ontologyPrefix);
						coherence.disposeReasoner();

						System.out.println("Generating ASP representation..:");
						String aspEncoding = coherence.getOntologyName()+".lp";
						coherenceGraph.toASPEncoding(aspLocation+"examples/"+aspEncoding);//createASPEncoding();

						System.out.println("Computing coherence degree...calling ASP");
						ArrayList<String> aspModels = ASPWrapper.callASP(aspLocation,aspExe,aspProblemEncoding,aspEncoding);
						coherenceGraph.fromASPEncoding(aspModels);

						//coherenceGraph.showCoherencePartitions();
						coherenceGraphs.put(coherence.getOntologyName(), coherenceGraph);
						//System.exit(1);
					}

				}
			}
			Iterator iterator = coherenceGraphs.entrySet().iterator();

			PrintWriter writer = new PrintWriter(aspLocation+"coherence-results.txt", "UTF-8");
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();

			writer.println("%%% AUTOMATICALLY GENERATED : "+dateFormat.format(date)+" do not change");
			writer.println();

			while (iterator.hasNext()) {

				Map.Entry pair = (Map.Entry)iterator.next();

				writer.println("CoherenceGraph for ontology "+pair.getKey());
				writer.println(((CoherenceGraph) pair.getValue()).showCoherencePartitions());

			}
			writer.close();


		} catch (OWLOntologyCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

}
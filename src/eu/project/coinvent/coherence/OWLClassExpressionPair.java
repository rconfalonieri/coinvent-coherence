package eu.project.coinvent.coherence;

import org.semanticweb.owlapi.model.OWLClassExpression;

public class OWLClassExpressionPair {

	private OWLClassExpression node1;
	private OWLClassExpression node2;
	
	public OWLClassExpressionPair(OWLClassExpression node1, OWLClassExpression node2) {
		this.node1 = node1;
		this.node2 = node2;
	}

	public OWLClassExpression getNode1() {
		return node1;
	}

	public void setNode1(OWLClassExpression node1) {
		this.node1 = node1;
	}

	public OWLClassExpression getNode2() {
		return node2;
	}

	public void setNode2(OWLClassExpression node2) {
		this.node2 = node2;
	}
	
	
}

package uk.ac.manchester.owl.owlapi.tutorialowled2011;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationObjectVisitorEx;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAnnotationPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLAnonymousIndividual;
import org.semanticweb.owlapi.model.OWLAsymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataComplementOf;
import org.semanticweb.owlapi.model.OWLDataExactCardinality;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataIntersectionOf;
import org.semanticweb.owlapi.model.OWLDataMaxCardinality;
import org.semanticweb.owlapi.model.OWLDataMinCardinality;
import org.semanticweb.owlapi.model.OWLDataOneOf;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLDataUnionOf;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLDatatypeDefinitionAxiom;
import org.semanticweb.owlapi.model.OWLDatatypeRestriction;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLDifferentIndividualsAxiom;
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLFacetRestriction;
import org.semanticweb.owlapi.model.OWLFunctionalDataPropertyAxiom;
import org.semanticweb.owlapi.model.OWLFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLHasKeyAxiom;
import org.semanticweb.owlapi.model.OWLInverseFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLIrreflexiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLNegativeDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLNegativeObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectExactCardinality;
import org.semanticweb.owlapi.model.OWLObjectHasSelf;
import org.semanticweb.owlapi.model.OWLObjectHasValue;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectInverseOf;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLObjectMinCardinality;
import org.semanticweb.owlapi.model.OWLObjectOneOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLObjectVisitorEx;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLReflexiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLSameIndividualAxiom;
import org.semanticweb.owlapi.model.OWLSubAnnotationPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLSubDataPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubPropertyChainOfAxiom;
import org.semanticweb.owlapi.model.OWLSymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLTransitiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.SWRLBuiltInAtom;
import org.semanticweb.owlapi.model.SWRLClassAtom;
import org.semanticweb.owlapi.model.SWRLDataPropertyAtom;
import org.semanticweb.owlapi.model.SWRLDataRangeAtom;
import org.semanticweb.owlapi.model.SWRLDifferentIndividualsAtom;
import org.semanticweb.owlapi.model.SWRLIndividualArgument;
import org.semanticweb.owlapi.model.SWRLLiteralArgument;
import org.semanticweb.owlapi.model.SWRLObjectPropertyAtom;
import org.semanticweb.owlapi.model.SWRLRule;
import org.semanticweb.owlapi.model.SWRLSameIndividualAtom;
import org.semanticweb.owlapi.model.SWRLVariable;

class LabelExtractor implements OWLObjectVisitorEx<String>, OWLAnnotationObjectVisitorEx<String> {

    @Override
    public String visit(OWLAnnotation node) {
        /*
         * If it's a label, grab it as the result. Note that if there are
         * multiple labels, the last one will be used.
         */
        if (node.getProperty().isLabel()) {
            OWLLiteral c = (OWLLiteral) node.getValue();
            return c.getLiteral();
        }
        return "";
    }

    public String doDefault(Object individual) {
        return "";
    }

	@Override
	public String visit(OWLDeclarationAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDatatypeDefinitionAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLAnnotationAssertionAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLSubAnnotationPropertyOfAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLAnnotationPropertyDomainAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLAnnotationPropertyRangeAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLSubClassOfAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLNegativeObjectPropertyAssertionAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLAsymmetricObjectPropertyAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLReflexiveObjectPropertyAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDisjointClassesAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataPropertyDomainAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectPropertyDomainAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLEquivalentObjectPropertiesAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLNegativeDataPropertyAssertionAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDifferentIndividualsAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDisjointDataPropertiesAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDisjointObjectPropertiesAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectPropertyRangeAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectPropertyAssertionAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLFunctionalObjectPropertyAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLSubObjectPropertyOfAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDisjointUnionAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLSymmetricObjectPropertyAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataPropertyRangeAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLFunctionalDataPropertyAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLEquivalentDataPropertiesAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLClassAssertionAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLEquivalentClassesAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataPropertyAssertionAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLTransitiveObjectPropertyAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLIrreflexiveObjectPropertyAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLSubDataPropertyOfAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLInverseFunctionalObjectPropertyAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLSameIndividualAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLSubPropertyChainOfAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLInverseObjectPropertiesAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLHasKeyAxiom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLRule arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLClass arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectIntersectionOf arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectUnionOf arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectComplementOf arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectSomeValuesFrom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectAllValuesFrom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectHasValue arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectMinCardinality arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectExactCardinality arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectMaxCardinality arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectHasSelf arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectOneOf arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataSomeValuesFrom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataAllValuesFrom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataHasValue arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataMinCardinality arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataExactCardinality arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataMaxCardinality arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDatatype arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataComplementOf arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataOneOf arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataIntersectionOf arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataUnionOf arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDatatypeRestriction arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLLiteral arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLFacetRestriction arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLObjectInverseOf arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLDataProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLAnnotationProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLNamedIndividual arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(IRI arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLAnonymousIndividual arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLClassAtom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLDataRangeAtom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLObjectPropertyAtom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLDataPropertyAtom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLBuiltInAtom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLVariable arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLIndividualArgument arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLLiteralArgument arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLSameIndividualAtom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(SWRLDifferentIndividualsAtom arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visit(OWLOntology arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}
